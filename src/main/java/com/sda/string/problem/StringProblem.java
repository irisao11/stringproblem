package com.sda.string.problem;

import java.util.Arrays;

public class StringProblem {
    //creating a boolean method that will return if all the words in my text have a reversed word in my text
    public boolean verifyWordsInText(String text) {
        // creating an array that contains the words in my text
        String toLowerText = text.toLowerCase();
        String[] stringToWords = toLowerText.split(" ");
        System.out.println(Arrays.toString(stringToWords));
        String[] reverseString = new String[stringToWords.length];
        //creating an array of ints that will count if a word has a reverse in my array of words
        int[] counts = new int[stringToWords.length];
        int count = 0;
        int count1;
        boolean verifyReverseWords = true;

        //creating an array of the reversed words in my text
        for (int i = 0; i < stringToWords.length; i++) {
            char[] eachWord = stringToWords[i].toCharArray();
            char[] reverseEachWord = new char[eachWord.length];
            for (int j = 0; j < eachWord.length; j++) {
                reverseEachWord[j] = eachWord[eachWord.length - j - 1];
            }
            //if a word is a palindrome, the count will become -1 , so it will not affect
            // the number of times a word appears in my text
            if (Arrays.equals(reverseEachWord, eachWord)) {
                count = count - 1;
                count1 = count;
                counts[i] = count1;
                count = 0;
            } else {
                count1 = 0;
                counts[i] = count1;
            }
            String reverseStringWord = new String(reverseEachWord);
            reverseString[i] = reverseStringWord;

        }
        System.out.println(Arrays.toString(reverseString));
        //counts[] gets the number of times a word appears in my text
        for (int i = 0; i < stringToWords.length; i++) {
            for (int j = 0; j < stringToWords.length; j++) {
                if (stringToWords[i].equals(reverseString[j])) {
                    counts[i] = counts[i] + 1;
                }
            }

        }
        System.out.println(Arrays.toString(counts));

        // asserting a value to the variable verifyReverseWords
        for (int i = 0; i < stringToWords.length; i++) {
            if (counts[i] != 1) {
                verifyReverseWords = false;
            }
        }
        System.out.println(verifyReverseWords);
        return verifyReverseWords;
    }

}
