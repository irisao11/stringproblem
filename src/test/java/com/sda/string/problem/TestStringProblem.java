package com.sda.string.problem;

import org.junit.Assert;
import org.junit.Test;

public class TestStringProblem {

    //testing a "true" result
    @Test
    public void verifyWordsInTextTest(){
        System.out.println(" verifyWordsInText was called");
        StringProblem stringProblem = new StringProblem();

        boolean verifyWords =stringProblem.verifyWordsInText("ana are mere erem era ana");
        Assert.assertTrue(verifyWords);
    }
    //testing a "false" result
    @Test
    public void verifyWordsInTextTestFalse(){
        System.out.println(" verifyWordsInTextFalse was called");
        StringProblem stringProblem = new StringProblem();

        boolean verifyWords =stringProblem.verifyWordsInText("ana are mere era ana");
        Assert.assertFalse(verifyWords);
    }

}
